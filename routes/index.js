var express = require("express");
var Notify = require("../models/notify");
var router = express.Router();
var functions = require("firebase-functions");
var admin = require("firebase-admin");
var serviceAccount = require("./notification-service-firebase-admin.json");
var uuid = require("uuid");
var CONFIG = require("../configs/configs");
const { ref } = require("firebase-functions/lib/providers/database");

const jwt = require("jsonwebtoken");
const jwksClient = require("jwks-rsa");

console.clear();


const authClient = jwksClient({
  	strictSsl: true, // Default value
	jwksUri: "https://accounts.vdatlab.com/auth/realms/vdatlab.com/protocol/openid-connect/certs",
	requestHeaders: {}, // Optional
	requestAgentOptions: {} // Optional
});
let signingKey;
authClient.getSigningKey(process.env.KID, (err, key) => {
	if (err) {
		console.log("GetSigningKey error " + err.toString());
	}
	signingKey = key.publicKey || key.rsaPublicKey;
	console.log("Signing Key Successful");
});



admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://notification-service-ee07d.firebaseio.com",
});

var database = admin.database();

var token_devices = {};

database.ref("/fcmTokens/").on(
	"value",
	function (snapshot) {
		token_devices = snapshot.val();
	},
	function (errorObject) {
		console.log("The read failed: " + errorObject.code);
	}
);

function onNotificationAdded(snapshot) {

	let notify = Notify.fromJson(snapshot.val());
	if (notify.status === CONFIG.STATUS.SENT) return;

	let listTargetDeviceToken = [];
	if (notify.target && notify.target.length > 0) {
		console.log("Has target");
		for (let userId of notify.target) {
			listTargetDeviceToken = [...new Set(listTargetDeviceToken.concat(token_devices[userId]))];
		}
	} else {
		console.log("Don't have target, send to all");
		for (let userId in token_devices) {
			listTargetDeviceToken = [...new Set(listTargetDeviceToken.concat(token_devices[userId]))];
		}
	}
	var payload = {
		notification: {
			title: notify.title,
			body: notify.msg_content,
		},
	};
	if (!listTargetDeviceToken || listTargetDeviceToken.length < 1) return;
	admin
		.messaging()
		.sendToDevice(listTargetDeviceToken, payload)
		.then(function (response) {
			console.log("Successfully sent message:", response);
		})
		.catch(function (error) {
			console.log("Error sending message:", error);
		});

	notify.status = CONFIG.STATUS.SENT;
	let parentNof = snapshot.ref.parent.key;
	let nofKey = snapshot.key;
	database.ref(`/notification/${parentNof}/${nofKey}`).set(notify.getObj());
}
database.ref("/notification/public").on("child_added", onNotificationAdded);
database.ref("/notification/private").on("child_added", onNotificationAdded);


/* GET home page. */
router.get("/", function (req, res, next) {
	res.render("index", { title: "Express" });
});
function jwtVerified(accessToken) {
	let authenticated = false;
	jwt.verify(accessToken, signingKey, function (err, userInfo) {
		if (err) {
			console.log("Error", err);
		} else {
			authenticated = true;
		}
	}
	);
	return authenticated;
}
router.post("/send-notify", async (req, res, next) => {
	try {
		const accessToken = req.headers["authorization"];
		if (!accessToken || !jwtVerified(accessToken.split(" ")[1]))
			return res.status(401).send("Unauthorized");

		let title = req.body.title;
		let msg_content = req.body.msg_content;
		let target = req.body.target;
		let notify = new Notify(
			id = uuid.v1(),
			title = title,
			msg_content,
			new Date().toUTCString(),
			[],
			CONFIG.STATUS.WAIT
		);
		console.log(target);
		if (target && target.length > 0) notify.target = target;

		let refDb = `/notification/${target ? "private" : "public"}/${notify.id}`;
		database.ref(refDb).set(notify.getObj());
		res.status(200).send("Sent notify successfully!");
	} catch (error) {
		res.status(500).send(error.toString());
	} finally {
		res.end();
	}


});

module.exports = router;
