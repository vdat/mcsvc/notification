var config = {
	API_ENDPOINT: {
		VERIFY_CAPTCHA: "https://www.google.com/recaptcha/api/siteverify",
		CREDENTIAL: "https://accounts.vdatlab.com/auth/realms/vdatlab.com/protocol/openid-connect/token",
		LIST_USER: "https://vdat-mcsvc-kc-admin-api-auth-proxy.vdatlab.com/auth/admin/realms/vdatlab.com/users?username=",
		SMS: "https://apis.vdatlab.com/sms/v1/sms",
		RESET_PASSWORD: "https://vdat-mcsvc-kc-admin-api-auth-proxy.vdatlab.com/auth/admin/realms/vdatlab.com/users/"
	},
	PAGE_ENDPOINT:{
		INDEX: "index",
		RESET_PASSWORD: "reset-password",

	},

	SERVER: "",
	MESSAGE_TYPE:{
		WARNING: "warning",
		SUCCESS: "success"
	},
	STATUS:{
		WAIT:"WAIT",
		SENT :"SENT"
	}
};


module.exports = config;