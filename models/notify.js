var notify = class Notify {
	constructor(id, title, msg_content, created_at, watched, status) {
		this.id = id;
		this.title = title;
		this.msg_content = msg_content;
		this.created_at = created_at;
		this.watched = watched;
		this.status = status;
	}
	getObj() {
		let objTemp = {
			"title": this.title,
			"msg_content": this.msg_content,
			"create_at": this.created_at,
			"watched": this.watched,
			"status": this.status
		};
		if (this.target) objTemp["target"] = this.target;

		return objTemp;

	}

	static fromJson(obj) {
		let notify = new Notify("", obj.title, obj.msg_content, obj.create_at, obj.watched ? obj.watched : [], obj.status);
		if (obj.target) notify.target = obj.target;
		return notify;
	}

};

module.exports = notify;
